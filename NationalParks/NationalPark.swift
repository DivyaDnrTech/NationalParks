//
//  NationalPark.swift
//  NationalParks
//
//  Created by Divya K on 3/5/17.
//  Copyright © 2017 DnRTech. All rights reserved.
//

import Foundation

class NationalPark {
    var name = ""
    var location = ""
    var imageName = ""
    
    init(name:String, location:String, imageName:String) {
        self.name = name    
        self.location = location
        self.imageName = imageName
    }
    
    static func allNationalParks() -> [NationalPark] {
        var result = [NationalPark]()
        result.append(NationalPark(name: "Acadia", location: "Maine", imageName: "Acadia"))
        result.append(NationalPark(name: "American Samoa", location: "American Samoa", imageName: "AmericanSamoa"))
        result.append(NationalPark(name: "Arches", location: "Utah", imageName: "Arches"))
        result.append(NationalPark(name: "Badlands", location: "South Dakota", imageName: "Badlands"))
        result.append(NationalPark(name: "Big Bend", location: "Texas", imageName: "BigBend"))
        result.append(NationalPark(name: "Biscayne", location: "Florida", imageName: "Biscayne"))
        result.append(NationalPark(name: "Black Canyon of the Gunnison", location: "Utah", imageName: "BlackCanyonGunnison"))
        result.append(NationalPark(name: "Bryce Canyon", location: "Utah", imageName: "BryceCanyon"))
        result.append(NationalPark(name: "Canyonlands", location: "Utah", imageName: "Canyonlands"))
        result.append(NationalPark(name: "Capitol Reef", location: "New Mexico", imageName: "CapitolReef"))
        result.append(NationalPark(name: "Channel Islands", location: "California", imageName: "ChannelIslands"))
        result.append(NationalPark(name: "Congaree", location: "South Carolina", imageName: "Congaree"))
        result.append(NationalPark(name: "Crater Lake", location: "Oregon", imageName: "CraterLake"))
        result.append(NationalPark(name: "Cuyahoga Valley", location: "Ohio", imageName: "CuyahogaValley"))
        result.append(NationalPark(name: "Death Valley", location: "California", imageName: "DeathValley"))
        result.append(NationalPark(name: "Denali", location: "Alaska", imageName: "Denali"))
        result.append(NationalPark(name: "Dry Tortugas", location: "Florida", imageName: "DryTortugas"))
        result.append(NationalPark(name: "Everglades", location: "Florida", imageName: "Everglades"))
        result.append(NationalPark(name: "Gates of the Arctic", location: "Maine", imageName: "GatesofArctic"))
        result.append(NationalPark(name: "Glacier", location: "Montana", imageName: "Glacier"))
        result.append(NationalPark(name: "Grand Canyon", location: "Arizona", imageName: "GrandCanyon"))
        result.append(NationalPark(name: "Grand Teton", location: "Wyoming", imageName: "GrandTeton"))
        result.append(NationalPark(name: "Great Basin", location: "Nevada", imageName: "GreatBasin"))
        result.append(NationalPark(name: "Guadalupe Mountains", location: "Texas", imageName: "GuadalupeMountains"))
        result.append(NationalPark(name: "Mammoth Cave", location: "Kentucky", imageName: "MammothCave"))
        result.append(NationalPark(name: "Redwood", location: "California", imageName: "Redwood"))
        result.append(NationalPark(name: "Sequoia", location: "California", imageName: "Sequoia"))
        return result
    }
}










