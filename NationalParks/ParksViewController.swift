//
//  ViewController.swift
//  NationalParks
//
//  Created by Divya K on 3/5/17.
//  Copyright © 2017 DnRTech. All rights reserved.
//

import UIKit

class ParksViewController: UITableViewController {
    var nationalParks = NationalPark.allNationalParks()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tableView.allowsSelection = false
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nationalParks.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        let title = nationalParks[indexPath.item].name
        let image = UIImage(named: nationalParks[indexPath.item].imageName)
        cell?.textLabel?.text = title
        cell?.imageView?.image = image
        return cell!
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}




